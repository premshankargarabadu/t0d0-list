var todoList = [];

function domRefresh() {
  let parentNode = document.getElementById('unorderlist');
  while (parentNode.firstChild) {
    parentNode.removeChild(parentNode.firstChild);
  }
  for (let i = 0; i < todoList.length; i++) {
    let newlist = document.createElement('li');
    newlist.className = 'todo-list-item';
    let mark = document.createElement('input');
    mark.className = 'todo-checkbox';
    mark.setAttribute('type', 'checkbox');
    mark.setAttribute(todoList[i]['completed'] ? 'checked' : 'unchecked', '');
    let inputValue = document.createElement('span');
    inputValue.className = 'listValue';
    inputValue.innerHTML = todoList[i]['value'];
    let deleteListItem = document.createElement('button');
    deleteListItem.className = 'todoRemove';
    deleteListItem.innerHTML = 'X';
    newlist.innerHTML += mark.outerHTML + inputValue.outerHTML + deleteListItem.outerHTML;
    newlist.setAttribute('index', i);
    document.getElementById('unorderlist').appendChild(newlist);
  }
}
var input = document.getElementById('input-value');
input.addEventListener('keydown', addValue);

function addValue(e) {
  if (e.which == 13) {
    let value = document.getElementById('input-value').value;
    if (value !== '') {
      addToDoItem(value, false);
      document.getElementById('input-value').value = '';
    }
  }
}

function addToDoItem(value, completed) {
  let item = {
    'value': value,
    'completed': completed
  };
  todoList.push(item);
  domRefresh();
}

function deleteToDoItem(event) {
  todoList.splice(event, 1);
  domRefresh();
}

function editInToDoList(event, editWith, completed) {
  todoList[event]['value'] = editWith;
  todoList[event]['completed'] = completed;
  domRefresh();
}
document.getElementById('unorderlist').addEventListener('click', editText);

function editText(e) {
  if (e.target.className === "todoRemove") {
    deleteToDoItem(e.target.parentNode.getAttribute('index'));
  }
  if (e.target.className === "todo-checkbox") {
    editInToDoList(e.target.parentNode.getAttribute('index'), e.target.parentNode.querySelector('.listValue').innerText, e.target.checked);
  }
}
